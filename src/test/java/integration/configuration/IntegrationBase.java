package integration.configuration;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class IntegrationBase {

    public static final String BASIC_URL;
    public static final Integer DEFAULT_WAITFOR_PAGE_SECONDS;
    public static final Integer DEFAULT_WAITFOR_AJAX_SECONDS;
    public static final String DEFAULT_SING_IN_EMAIL;
    public static final String DEFAULT_SING_IN_PASSW;

    static {
        BASIC_URL = "http://fortesting.info/";
        DEFAULT_WAITFOR_PAGE_SECONDS = 30;
        DEFAULT_WAITFOR_AJAX_SECONDS = 30;
        DEFAULT_SING_IN_EMAIL = "User1@mailforspam.com";
        DEFAULT_SING_IN_PASSW = "123456";
    }

    protected WebDriver driver;
    protected String browser;

    public IntegrationBase(String browser) {
        this.browser = browser;
    }

    @BeforeMethod
    public final void setup() {
        driver = createWebDriver();
    }

    private WebDriver createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\driver\\chromedriver.exe");
        driver = new ChromeDriver();
        return driver;
    }

    public void openBasicHomePage(String url) {
        driver.navigate().to(url);
    }
    public void enterSingInEmail(String firstEmail){
        driver.findElement(By.id("Email")).clear();
        driver.findElement(By.id("Email")).sendKeys(firstEmail);
    }
    public void enterSingInPassw(String firstPassw){
        driver.findElement(By.id("Password")).clear();
        driver.findElement(By.id("Password")).sendKeys(firstPassw,Keys.ENTER);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    @AfterMethod
    public final void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
