package integration.tests;


import integration.configuration.IntegrationBase;
import integration.pagemodels.*;
import integration.pagemodels.common.EnumLogInCredentions;
import integration.pagemodels.simpleUser.CartPage;
import integration.pagemodels.simpleUser.HomePageJustUser;
import integration.pagemodels.simpleUser.PharmacyPageOfSimpleUser;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SingleUserTests extends IntegrationBase{
    private SingInPage singInPage;
    private LogInPage logInPage;
    private HomePageJustUser homePageJustUser;
    private PharmacyPageOfSimpleUser pharmacyPage;
    private CartPage cartPage;

    public SingleUserTests(String browser) {
        super(browser);
    }
    @BeforeMethod
    public void setupSignInPagetext() {
        singInPage = new SingInPage(driver);
        openBasicHomePage(BASIC_URL);
        singInPage.waitForSingToLoad();
        enterSingInEmail(DEFAULT_SING_IN_EMAIL);
        enterSingInPassw(DEFAULT_SING_IN_PASSW);
        logInPage = new LogInPage(driver);
        homePageJustUser = new HomePageJustUser(driver);
        pharmacyPage = new PharmacyPageOfSimpleUser(driver);
        cartPage = new CartPage(driver);

    }


   @Test
    public void simpleleUserLogIn() {
       logInPage.waitForPageToLoad();
       logInPage.currentUserLogIn(EnumLogInCredentions.SINGLE_Email);
       homePageJustUser.waitForHomePageToLoad();
       homePageJustUser.verifyIfJustUserIsReallyLogIn(true);
       homePageJustUser.goToPharmacyFromHomemPAge();
       pharmacyPage.puttingMedicalsInCart();
       cartPage.verifyStutusOfProductBeforeBuying();
       cartPage.ifRemoteButtonIsEnabelInCard();
       cartPage.verifyIfstutusChangesAfterBuying(true);
       cartPage.IfUserIsAbleToLogOut();
       cartPage.logOutSingleUser();
    }
}
