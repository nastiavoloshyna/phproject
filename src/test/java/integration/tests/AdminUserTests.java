package integration.tests;

import integration.configuration.IntegrationBase;
import integration.pagemodels.LogInPage;
import integration.pagemodels.SingInPage;
import integration.pagemodels.adminUser.CreateNewAccountPage;
import integration.pagemodels.adminUser.HomePageAdminUser;
import integration.pagemodels.common.EnumLogInCredentions;
import integration.pagemodels.simpleUser.HomePageJustUser;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AdminUserTests extends IntegrationBase {

    private SingInPage singInPage;
    private LogInPage logInPage;
    private HomePageAdminUser homePageAdminUser;
    private CreateNewAccountPage createNewAccountPage;


    public AdminUserTests(String browser) {
        super(browser);
    }

    @BeforeMethod
    public void setupSignInPagetext() {
        singInPage = new SingInPage(driver);
        openBasicHomePage(BASIC_URL);
        singInPage.waitForSingToLoad();
        enterSingInEmail(DEFAULT_SING_IN_EMAIL);
        enterSingInPassw(DEFAULT_SING_IN_PASSW);
        logInPage = new LogInPage(driver);
        homePageAdminUser = new HomePageAdminUser(driver);
        createNewAccountPage = new CreateNewAccountPage(driver);
    }

    @Test
    public void adminUserLogIn() {
        logInPage.waitForPageToLoad();
        logInPage.currentUserLogIn(EnumLogInCredentions.ADMIN_Email);
        homePageAdminUser.waitForHomePageToLoad();
        homePageAdminUser.goToUsersAddButton();
        homePageAdminUser.waitForHomePageToLoad();
        createNewAccountPage.fillUpRequiredFieldsForNewAccout();
    }
}
