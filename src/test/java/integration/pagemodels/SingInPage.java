package integration.pagemodels;


import integration.configuration.AbstractWebComponents;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SingInPage extends AbstractWebComponents{

    @FindBy (id = "loginbox")
    private WebElement singPageFooter;
    public SingInPage(WebDriver driver) {
        super(driver);
    }
    public void waitForSingToLoad() {
        waitForPage.until(ExpectedConditions.visibilityOf(singPageFooter));
    }
}
