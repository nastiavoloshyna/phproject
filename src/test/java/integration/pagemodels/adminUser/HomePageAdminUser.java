package integration.pagemodels.adminUser;


import integration.configuration.AbstractWebComponents;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class HomePageAdminUser extends AbstractWebComponents {
    @FindBy(css = (".row"))
    private WebElement homePageFooter;
    @FindBy (xpath = ("//a[contains(text(),'Users')]"))
    private WebElement userButtonInToolbar;
    @FindBy (xpath = ("//a[contains(text(),'Add User')]"))
    private WebElement addUserButtonInDropDownList;

    public HomePageAdminUser(WebDriver driver) {
        super(driver);
    }
       //Methods
    public void waitForHomePageToLoad() {
        waitForPage.until(ExpectedConditions.visibilityOf(homePageFooter));
    }

    public boolean verifyIfUserButtonIsEnable(){
        return userButtonInToolbar.isEnabled();
    }
    public boolean ifAddButtonIsEnable(){
        return addUserButtonInDropDownList.isEnabled();
    }

    public void goToUsersAddButton(){
        if(verifyIfUserButtonIsEnable()) {
            userButtonInToolbar.click();
            if (ifAddButtonIsEnable()){
                addUserButtonInDropDownList.click();
                driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            }else {Assert.fail("Drop-down list is disable!");}
        }else {Assert.fail("Users button is disable!");}
    }

}
