package integration.pagemodels.adminUser;

import integration.configuration.AbstractWebComponents;
import integration.pagemodels.common.EnumNewAccountInformation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

import static integration.pagemodels.common.EnumNewAccountInformation.*;

public class CreateNewAccountPage extends AbstractWebComponents{

    @FindBy (id="Email")
    private WebElement newUserEmail;
    @FindBy (id="Password")
    private WebElement newUserPassw;
    @FindBy (id="ConfirmPassword")
    private WebElement confirmPassw;
    @FindBy (id="FirstName")
    private WebElement newUserFirstname;
    @FindBy (id="LastName")
    private WebElement newUserLastname;
    @FindBy (id="PhoneNumber")
    private WebElement newUserPhone;
    @FindBy (id="IsAdmin")
    private WebElement checkboxIsAdmin;
    @FindBy (css = (".btn.btn-success"))
    private WebElement registerButton;

    public CreateNewAccountPage(WebDriver driver) {
        super(driver);
    }

    public void fillUpRequiredFieldsForNewAccout(){
        String userFNameEnum = String.valueOf(FIRST_NAME);
        String userLNameEnum = String.valueOf(LAST_NAME);
        String userEmailEnum = String.valueOf(USER_EMAIL);
        String userPasswEnum = String.valueOf(USER_PASSW);
        String userPhoneEnum = String.valueOf(PHONE);
        newUserEmail.sendKeys(userEmailEnum);
        newUserPassw.sendKeys(userPasswEnum);
        newUserFirstname.sendKeys(userFNameEnum);
        newUserLastname.sendKeys(userLNameEnum);
        newUserPhone.sendKeys(userPhoneEnum);
        confirmPassw.sendKeys(userPasswEnum);
        checkboxIsAdmin.click();
        registerButton.click();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }


}
