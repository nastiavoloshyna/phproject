package integration.pagemodels.simpleUser;

import integration.configuration.AbstractWebComponents;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.security.PublicKey;
import java.util.concurrent.TimeUnit;

public class CartPage extends AbstractWebComponents{

    String shoppingCartTextOnPage = "Shopping Cart";

    @FindBy (xpath = ("//tr[2]/td[6]"))
    private WebElement statusCurrentProduct;
    @FindBy (css = (".btn.btn-danger"))
    private WebElement remoteButton;
    @FindBy (css = (".btn.btn-success"))
    private WebElement buyCartButton;
    @FindBy (css=(".container.body-content>h4"))
    private WebElement shoppingCartText;
    @FindBy (xpath = ("//a[contains(text(),'Log out')]"))
    private WebElement logOutButton;

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public void verifyStutusOfProductBeforeBuying() {
        if (statusCurrentProduct.equals("Pending")) {
            buyCartButton.click();
        } else {
            Assert.fail("Product is already bought");
        }
    }
    public void IfShoppingCartPageIsVisible(String expectedResult){
        Assert.assertEquals(shoppingCartText.getText(),expectedResult);
    }

    public boolean ifRemoteButtonIsEnabelInCard() {
        return remoteButton.isEnabled();
    }

    public void verifyIfstutusChangesAfterBuying(boolean expectedResult){
        IfShoppingCartPageIsVisible(shoppingCartTextOnPage);
        boolean actualResult = statusCurrentProduct.getText().contains("Paid");
        if (actualResult) {
            Assert.assertEquals(actualResult, expectedResult, "Stutus changed to Paid ");
        } else {Assert.fail("Status is not changed");}
    }
    public boolean IfUserIsAbleToLogOut(){
        remoteButton.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return logOutButton.isEnabled();
    }
    public void logOutSingleUser(){
        logOutButton.click();
    }

}
