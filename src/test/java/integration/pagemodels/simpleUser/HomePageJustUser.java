package integration.pagemodels.simpleUser;


import integration.configuration.AbstractWebComponents;
import integration.pagemodels.common.EnumLogInCredentions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import javax.management.openmbean.ArrayType;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import static java.lang.System.out;

public class HomePageJustUser extends AbstractWebComponents {

    @FindBy(css = (".row"))
    private WebElement homePageFooter;
    @FindBy(css = ("a[title=\"Manage\"]"))
    private WebElement possitionOfUserEmail;
    @FindBy(css = (".btn.btn-primary"))
    private WebElement oneOfPharmacyDetail;

    public HomePageJustUser(WebDriver driver) {
        super(driver);
    }

    //Methods
    public void waitForHomePageToLoad() {
        waitForPage.until(ExpectedConditions.visibilityOf(homePageFooter));
    }

    public void verifyIfJustUserIsReallyLogIn(boolean expectedResult) {
        boolean actualResult = possitionOfUserEmail.getText().contains("JustUser4@mailforspam.com");
        if (actualResult) {
            Assert.assertEquals(actualResult, expectedResult, "Current user is JustUser");
        } else {
            Assert.fail("Unexpected user has logged!");
        }
    }
    /*public String randomPharmacy() {
        Random random = new Random();
        JavascriptExecutor myjse = (JavascriptExecutor) driver;
        Object arrayPharmace = myjse.executeScript("return.document.getgetElementsByTagName('p')");
        int randomIndex = random.nextInt(arrayPharmace.());
        return arrayPharmace(randomIndex);
    }*/

    public void goToPharmacyFromHomemPAge() {
        oneOfPharmacyDetail.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }



}
