package integration.pagemodels.simpleUser;

import integration.configuration.AbstractWebComponents;
import javafx.scene.web.WebView;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class PharmacyPageOfSimpleUser extends AbstractWebComponents {

    public String zero = "0";

    @FindBy (xpath = ("//a/following::*[contains(text(),\"Products\")]"))
    private WebElement productsTab;
    @FindBy (css = (".btn.btn-success"))
    private WebElement buyButton;
    @FindBy (css = ("tr"))
    private WebElement rowOfProduct;
    @FindBy(xpath = ("//a[contains(text(),'Cart')]"))
    private WebElement cartButton;
    @FindBy (xpath = ("//tr[2]"))
    private WebElement rowInCart;
    @FindBy (id="Pharmacy_Name")
    private WebElement pharmacyName;

    public PharmacyPageOfSimpleUser(WebDriver driver) {
        super(driver);
    }

    public void puttingMedicalsInCart() {
       String currentPharmacy = pharmacyName.getText();
        productsTab.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if (ifBuyButtonIsEnabel()) {
            if (rowOfProduct.findElement(By.xpath("//td[3]")).getText() != zero) {
                String nameOfProduct = rowOfProduct.findElement(By.xpath("//td[1]")).getText();
                String description = rowOfProduct.findElement(By.xpath("//td[2]")).getText();
                String prise = rowOfProduct.findElement(By.xpath("//td[4]")).getText();
                buyButton.click();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                cartButton.click();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                String nameOfProductInCart = rowOfProduct.findElement(By.xpath("//td[1]")).getText();
                String descriptionProductInCart = rowOfProduct.findElement(By.xpath("//td[2]")).getText();
                String priseInCart = rowOfProduct.findElement(By.xpath("//td[4]")).getText();
                String pharmacyInCart = rowOfProduct.findElement(By.xpath("//td[5]")).getText();
                Assert.assertEquals(nameOfProductInCart, nameOfProduct, "Name is correct");
                Assert.assertEquals(descriptionProductInCart, description, "Description is correct");
                Assert.assertEquals(priseInCart, prise, "Prise is correct");
                Assert.assertEquals(pharmacyInCart, currentPharmacy, "Pharmacy is correct");
            } else {Assert.fail("Incorrect, Count equels zero");}
        } else {Assert.fail("Buy botton is disable");}
        String countInCart = rowOfProduct.findElement(By.xpath("//td[3]")).getText();
        if(countInCart.equals("1")){
            System.out.println("Added count of product equels" + countInCart);
        }else {Assert.fail("Incorrect, Count doesn't equels one, and equels="+countInCart);}
    }

    public boolean ifBuyButtonIsEnabel() {
        return buyButton.isEnabled();
    }
}






