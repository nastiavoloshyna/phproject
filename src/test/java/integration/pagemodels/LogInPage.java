package integration.pagemodels;

import integration.configuration.AbstractWebComponents;
import integration.pagemodels.common.EnumLogInCredentions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import sun.rmi.runtime.Log;

import java.util.concurrent.TimeUnit;

import static java.lang.System.out;

public class LogInPage extends AbstractWebComponents{

    private String generalPassw="!1234Qwer";

    @FindBy(id = "loginForm")
    private WebElement logInPageForm;

    //Login navigate values
    @FindBy (id = "Email")
    private WebElement logInFormEmail;
    @FindBy (id = "Password")
    private WebElement logInFormPassw;
    @FindBy (css = (".btn.btn-success"))
    private WebElement submitButton;

    public LogInPage(WebDriver driver) {
        super(driver);
    }
    //Methods
    public void waitForPageToLoad() {
        waitForPage.until(ExpectedConditions.visibilityOf(logInPageForm));
    }

    //log in
    public void currentUserLogIn(EnumLogInCredentions enumLogInCredentions){
        if (logInFormEmail.isEnabled() & logInFormPassw.isEnabled()){
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
            switch (enumLogInCredentions){
                case SINGLE_Email:
                    logInFormEmail.sendKeys("JustUser4@mailforspam.com");break;
                case MANAGER_Email:
                    logInFormEmail.sendKeys("Manager4@mailforspam.com");break;
                case ADMIN_Email:
                    logInFormEmail.sendKeys("Admin4@mailforspam.com");break;
            }
            logInFormPassw.sendKeys(generalPassw);
            submitButton.click();
            driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        } else {
            Assert.fail("Fail,section is busy");
        }
    }

}
